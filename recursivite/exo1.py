def fact(n):
    """Renvoie la factorielle de n en utilisant une approche récursive"""
    return 0


def test_fact():
    assert fact(0) == 1
    assert fact(1) == 1
    assert fact(2) == 2
    assert fact(3) == 6
    assert fact(4) == 24
    assert fact(5) == 120


test_fact()

def compte_n(chaine):
    """Renvoie le nombre de caractères dans la chaine passée en paramètre en
        utilisant une approche récursive."""
    return 0

def test_compte_n():
    assert compte_n("salut") == 0
    assert compte_n("non") == 3
    assert compte_n("nnnnnn") == 6


