
import piquet

def creer_hanoi(n):
    piquet1 = piquet.creer()
    piquet2 = piquet.creer()
    piquet3 = piquet.creer()

    for i in range(n, 0, -1):
        piquet.empiler(i, piquet1)

    return [piquet1, piquet2, piquet3]


def afficher_matrice(m):
    """Fonction affichant une matrice m où chaque case de m est un unique
       caractère (elle est déjà codé, il n'y a rien à faire :) ).

       Par exemple:

       >>> afficher_matrice([ ['s', ' ', ' n'], [' ', 'o', ' '], ['n', ' ', 's']])
       s n
        o
       n s

    """
    for ligne in m:
        for symbole in ligne:
            print(symbole, end="")
        print()



def afficher_s():
    """Pour se familiariser avec la fonction afficher_matrice, utilisez
    la fonction précédente pour afficher le S suivant:

    ###############
    #
    #
    #
    #
    #
    #
    #
    ###############
                  #
                  #
                  #
                  #
                  #
                  #
    ###############

    (utilisez des boucles!)
    """
    # creation d'une matrice de 15 colonnes et de 16 lignes remplie d'espaces
    matrice = [[' '] * 15 for i in range(16)]

    # ...
    # code à écrire ici !
    # ...

    afficher_matrice(matrice)


def concatene_hori(m1, m2):
    """Concatene horizontallement des matrices m1 et m2 ayant le même nombre de
    lignes et renvoi la matrice correspondante.
    Par exemple:

    >>> concatene_hori([[1, 2], [3, 4]], [[5],    [6]])
    [[1, 2, 5], [3, 4, 6]]
    """
    return []

def afficher_hanoi(piquets):
    """Affiche l'état du jeu de hanoi. L'argument piquets contient
    trois piquets.


    La fonction `piquet.afficher_dans_matrice(largeur, hauteur, p)`
    renvoie une matrice de dimension largeur x hauteur où est représentée le
    piquet p avec des étoiles (*).

    On utilisera une largeur de 13 et une hauteur de 6 pour les 3 piquets.
    """

    # On finit par une ligne horizontale :)
    print("_" * 13 * 3)

# Pour tester la fonction afficher_hanoi, à (dé)commenter
# afficher_hanoi(creer_hanoi(5))

def changer_disque(piquet_depuis, piquet_vers):
    """Prend le disque sur le sommet de piquet_depuis et l'empile sur le
       piquet_vers.

       Ne renvoie rien, modifie l'état des piquets piquet_depuis et piquet_vers.
    """
    pass


def test_changer_disque():
    piquet1 = piquet.creer()
    piquet2 = piquet.creer()

    piquet.empiler(1, piquet1)
    changer_disque(piquet1, piquet2)

    assert piquet.est_vide(piquet1) and piquet.sommet(piquet2) == 1

    print("test_changer_disque passé avec succès")

test_changer_disque()

def resoudre_hanoi(piquets, n, depuis, vers, tampon):
    """Résoud le jeu de hanoi. Arguments:
    * piquets: la liste des trois piquets,
    * n: le nombre de disques qu'on veut déplacer,
    * depuis: l'indice du piquet depuis lequel on veut déplacer les disques
    * vers: l'indice du piquet vers lequel on veut déplacer les disques
    * tampon: l'indice du piquet qu'on peut utiliser en "tampon" pour stocker
              des disques.

    Si on a créé un jeu avec 5 disques, on appelera donc:
    resoudre_hanoi(piquets, 5, 0, 2, 1)

    On affichera l'état du jeu grâce à la fonction afficher_hanoi à chaque fois
    qu'on change un disque de place.
    """
    pass

def jeu(n):
    piquets = creer_hanoi(n)
    afficher_hanoi(piquets)
    resoudre_hanoi(piquets, n, 0, 2, 1)

# à décommenter quand vous avez tout fini! Lancez jeu(5), jeu(10)...
#jeu(3)