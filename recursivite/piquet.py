"""
Module implémentant une structure de piquet de tour de Hanoi.

Un piquet est une pile où on ne peut empiler des éléments que s'ils sont
plus petits que sur le sommet.
"""

class PiquetVideError(Exception):
    """Exception indiquant qu'on tente d'accéder à un élément d'un piquet vide"""
    pass

class EmpilerTropGrandError(Exception):
    """Exception indiquant qu'on tente d'empiler un élément plus petit que celui
       au sommet d'un piquet"""
    pass

def creer():
    """Renvoie une nouveau piquet vide"""
    return []

def empiler(objet, piquet):
    """Modifie le piquet en ajoutant l'objet à son sommet. Ne renvoie rien.
    Lance l'erreur EmpilerTropGrandError si l'objet empilé est plus grand
    que l'objet au sommet.
    """
    if not est_vide(piquet) and  objet >= sommet(piquet) :
        raise EmpilerTropGrandError()
    piquet.append(objet)

def depiler(piquet):
    """Modifie le piquet en retirant l'objet sur le haut du piquet et renvoie cet
    objet. Lance l'erreur PileVide si la   piquet est vide. Ne renvoie rien."""
    if est_vide(piquet):
        raise PileVideError()

    return piquet.pop()

def sommet(piquet):
    """Renvoie l'objet au somme du piquet, sans modifier le piquet. Lance
    l'erreur PiquetVideError si le piquet est vide."""
    if est_vide(piquet):
        raise PileVide()

    return piquet[-1] # renvoie le dernier élément de la liste.


def est_vide(piquet):
    """Renvoie True si le piquet est vide, et False sinon. Ne modifie pas le
    piquet."""
    return piquet == []

def afficher_dans_matrice(largeur, hauteur, piquet):
    screen = [[" "]* largeur for i in range(hauteur)]

    x_milieu = largeur // 2 + 1

    for i, taille in enumerate(piquet):
        if i >= hauteur:
            break
        screen[hauteur - i - 1][x_milieu] = "*"
        for j in range(taille):
            if x_milieu + j >= largeur or x_milieu - j < 0:
                break
            screen[hauteur - i - 1][x_milieu + j] = "*"
            screen[hauteur - i - 1][x_milieu - j] = "*"

    for i in range(len(piquet), hauteur):
        screen[hauteur - i - 1][x_milieu] = "|"

    return screen