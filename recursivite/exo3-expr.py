"""Une expression est modélisée par:
* soit un nombre
* soit une liste à 3 éléments [seg, op, sed] où seg et sed sont des sous
  expressions, et op est un opérateur ("+", "*", "-" ou "/")
"""


def evaluer(expr):
    """Evalue une expression. Renvoie un nombre."""
    return 0

def test_evaluer():
    assert evaluer(5) == 5
    assert evaluer([5, "+", 3]) == 8
    assert evaluer([5, "+", [4, "*", 2]]) == 13

    print("test_evaluer passé avec succès")

test_evaluer()

"""On ajoute à nos expression la possibilité d'avoir des variables.
Par exemple "x + 3" est représenté par :
 ["x", "+", 3]


"""

def deriver(expr, var):
    """Dérive une expression par rapport à une variable donnée.
    Renvoie une autre expression.
    Par exemple

    deriver([[3, "*", "x"], "+", "t"], "x")

    doit renvoyer une expression équivalente à 3.

    et

    deriver([[3, "*", "x"], "+", "t"], "t")

    doit renvoyer une expression équivalente à 1.
    """
    return expr

# Dérivons... c'est un peu compliqué !
print(deriver([[[3, "*", "x"], "+", "t"], '/', ['x', '+', 1]], "x"))
print(deriver([[3, "*", "x"], "+", "t"], "t"))



def simplifier(expr):
    """Simplifie une expression. Renvoie une autre expression (voir les tests
    pour l'ensemble des cas à gérer)."""
    return expr

def test_simplifier():

    # cas de base
    assert simplifier(["x", "+", 0]) == "x"
    assert simplifier([0, "+", "x"]) == "x"

    assert simplifier(["x", "-", 0]) == "x"
    assert simplifier([5, "-", 5]) == 0

    assert simplifier([0, "*", "x"]) == 0
    assert simplifier([1, "*", "x"]) == "x"
    assert simplifier(["x", "*", 1]) == "x"

    assert simplifier(["x", "/", 1]) == "x"
    assert simplifier([0, "/", "x"]) == 0
    assert simplifier([3, "/", 3]) == 1

    # cas imbriqué
    assert simplifier([["x", "+", 0], "/", ["x", "*", 1]]) == 1
    assert simplifier([["x", "*", ["y", "/", "y"]], "-", [0, "+", "x"]]) == 0
    assert simplifier([["x", "+", 0], "*", ["y", "/", 1]]) == ["x", "*", "y"]

    print("test_simplifier passé avec succès")

test_simplifier()

# Dérivons, puis simplifions...
print(simplifier(deriver([[[3, "*", "x"], "+", "t"], '/', ['x', '+', 1]], "x")))
print(simplifier(deriver([[3, "*", "x"], "+", "t"], "t")))

print(simplifier([["x", "+", 3], "/", ["x", "+", 3]]))