"""
Module implémentant une structure de pile "cyclique": une fois pleine, le fond
de la pile est remplacée par les nouvelles valeurs ajoutées. 


L'utilisateur de ce module doit uniquement utiliser les fonctions
creer, est_vide, est_pleine, empiler, empiler, depiler et sommet.

Ci-dessous est expliqué le fonctionnement interne de la structure de données
sur laquelle l'utilisateur ne doit pas s'appuyer (cette partie est là dans un 
but uniquement pédagogique).

On représente une pile cyclique grâce à une liste fixe et deux index:
 * index_top: indiquant l'index du dernier élément inséré;
 * index_bottom: indiquant l'index du fond de la pile. 

La pile est vide si index_top == -1.

Par exemple, créer une pile d'une capacité 5 créé la liste suivante (la flèche
du bas représente index_top et la flèche du haut représente index_bottom):

       |
       v
 
    | None | None | None | None | None |
 
  ^
  |


Après avoir inséré un élément (mettons 42), la pile se représente par (la flèche
index_top a avancé, alors qu'index bottom reste où il est) :

       |
       v

    |  42  | None | None | None | None |
 
       ^
       |


Insérons encore un élément (mettons 91), la pile se représente par:

       |
       v

    |  42  |  91  | None | None | None |
 
               ^
               |


Maintenant, dépilons le sommet (la liste reste inchangée, juste
index_top bouge; la valeur 91 n'est pas effacée pour l'instant
même si elle n'est plus accessible):

        |
        v

    |  42  |  91  | None | None | None |
 
        ^     
        |

Insérons maintenant une nouvelle valeur (mettons 56): la valeur est insérée
en écrasant le 91 précédent.

        |
        v

    |  42  |  56  | None | None | None |
 
              ^     
              |

Si on continue à insérer des valeurs, on obtiendra par exemple :


        |
        v

    |  42  |  56  |  86 | 13  | 74 |
 
                                ^     
                                |

Maintenant la pile est remplie, si on ajoute un élément (mettons 58), on va
venir écraser le premier inséré (à savoir 42) et déplacer les deux flèches:

               |
               v

    |  58  |  56  |  86 | 13  | 74 |
 
        ^     
        |

Empilons encore trois éléments (mettons 12, 35 et 46):

                                |
                                v

    |  58  |  12  |  35 | 46  | 74 |

                           ^     
                           |


Si on empile encore un élément (mettons 69), index_bottom revient au début:

        |
        v

    |  58  |  12  |  35 | 46  | 69 |

                                ^     
                                |


Empilons 81, index_top "fait le tour" alors que index_bottom avance "normalement"

               |
               v

    |  81  |  12  |  35 | 46  | 69 |

        ^     
        |


Dépilons un élément, la liste ne bouge pas, juste index_top (dans cette 
situation, 81 n'est pas effacé mais est inaccessible):

               |
               v

    |  81  |  12  |  35 | 46  | 69 |
                                 
                                ^
                                |


Si on dépile encore 3 éléments, on obtient :

               |
               v

    |  81  |  12  |  35 | 46  | 69 |
                                 
               ^
               |


Dans cette situation la pile comprend un seul élément: 12 (dans ce cas, les deux
flèches pointent sur le même élément; toutes les autres
valeurs ne sont pas effacées (cela coûterait du temps!), les positions des
flèches indiquent que ces valeurs sont insignifiantes.


En dépilant ce dernier élément, on obtient (la flèche index_top est mise à "-1"):

               |
               v

    |  58  |  12  |  35 | 46  | 69 |

 ^
 |

Autrement dit, la pile est vide ! Les données présentes ne veulent rien dire ;)
"""

class PileVide(Exception):
    """Exception indiquant qu'on tente d'accéder à un élément d'une pile vide"""
    pass

def creer(capacite):
    """Renvoie une nouvelle pile cyclique vide d'une capacité donnée"""
    if type(capacite) != int:
        raise ValueError('capacite doit être un entier')
    if capacite <= 0:
        raise ValueError('capacite doit être strictement positif')
    
    return {
        'capacite': capacite,
        'elements': [None] * capacite,
        'index_top': -1,
        'index_bottom': 0,
    }


def est_vide(pile):
    """Renvoie True si la pile est vide, et False sinon. Ne modifie pas la 
    pile."""
    return False

def empiler(objet, pile):
    """Modifie la pile en ajoutant l'objet à son sommet. Ne renvoie rien."""
    # Si la pile est vide, réinitialiser les index à 0 et ajoutez le nouvel objet.
    # Sinon calculer le nouveau index_top, vérifiez s'il faut modifier
    # index_bottom et ajoutez le nouvel objet
    pass

def depiler(pile):
    """Modifie la pile en retirant l'objet sur le haut de la pile et renvoie cet
    objet. Lance l'erreur PileVide si la   pile est vide. Ne renvoie rien."""
    # S'il n'y a qu'un seul élément dans la pile, pensez à marquer la pile comme
    # vide.
    pass


def to_list(pile):
    """Renvoie la pile sous forme de liste, en incluant uniquement les éléments
    "valides" (et pas les éléments dépilés). L'ordre doit être l'ordre inverse
    de celui d'empilage (les derniers éléments ajoutés seront à la fin de la 
    liste)."""
    return []




def sommet(pile):
    """Renvoie l'objet au somme de la pile, sans modifier la pile. Lance
    l'erreur PileVide si la   pile est vide."""
    return None


