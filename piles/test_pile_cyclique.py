from pile_cyclique import creer, empiler, depiler, est_vide, sommet, to_list

failed = False 

p = creer(5)

if not est_vide(p):
    print("ERREUR : la pile est indiquée vide à sa création...")
    failed = True

if to_list(p) != []:
    print("ERREUR : la conversion en liste donne", to_list(p), "au lieu de: []")
    failed = True

empiler(42, p)

empiler(91, p)

e1 = depiler(p)

if e1 != 91:
    print("ERREUR : premier élément dépilé:", e1, "attendu: ", 91)
    failed = True


empiler(56, p)

empiler(86, p)

empiler(13, p)

empiler(74, p)

s1 = sommet(p)

if s1 != 74:
    print("ERREUR : mauvais sommet :", s1, "attendu: ", 74)
    failed = True

empiler(58, p)

if to_list(p) != [56, 86, 13, 74, 58]:
    print("ERREUR : la conversion en liste donne", to_list(p), "au lieu de: [56, 86, 13, 74, 58]")
    failed = True

empiler(12, p)

empiler(35, p)

empiler(46, p)

empiler(69, p)

empiler(81, p)

e2 = depiler(p)

if e2 != 81:
    print("ERREUR : deuxième élément dépilé:", e2, "attendu: ", 81)
    failed = True

depiler(p)
depiler(p)
depiler(p)

if to_list(p) != [12]:
    print("ERREUR : la conversion en liste donne", to_list(p), "au lieu de: [12]")
    failed = True

if est_vide(p):
    print("ERREUR : la pile est indiquée vide alors qu'elle n'est pas censée l'être !")
    failed = True

e3 = depiler(p)

if e3 != 12:
    print("ERREUR : troisième élément dépilé:", e3, "attendu: ", 12)
    failed = True

if not est_vide(p):
    print("ERREUR : la pile est indiquée non vide alors qu'elle doit l'être !")
    failed = True

if not failed:
    print("test pile_cyclique passé avec succès")
