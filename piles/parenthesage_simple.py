def est_bien_parenthese(mot):
    """Renvoie vrai si le mot est bien parenthésé, faux sinon. Ne prends
    en compte que les symbole "(" et ")", ignore tous les autres."""
    return True


def test_est_bien_parenthese():
    assert not est_bien_parenthese("(")
    assert not est_bien_parenthese(")")
    assert not est_bien_parenthese("a+(")
    assert not est_bien_parenthese("a)+(")
    assert not est_bien_parenthese("(()")
    assert not est_bien_parenthese(")()(")
    assert not est_bien_parenthese("())")

    assert est_bien_parenthese("()")
    assert est_bien_parenthese("()(()(()))")
    assert est_bien_parenthese("(((((((((((((((((())))))))))))))))))")

    print("test est_bien_parenthese passé avec succès")

test_est_bien_parenthese()
