from pile import empiler, depiler, creer

def est_op(chaine):
    """Prend une chaîne de un caractère en argument et qui renvoie True si le
       caractère est un symbole d'opération parmi +, -, /,* et False sinon.
    """
    return False


def str_to_lexemes(chaine):
    """
    Prend en argument une chaîne de caractère composée uniquement d'espaces, de
    chiffres et des symboles d'opération définissant une expression
    postfixée valide; cette fonction doit renvoyer la liste des
    lexèmes de la chaîne de caractères.
    Par exemple:
    str_to_lexeme('42 2 2 + *') renvoie la liste [42., 2., 2., '+', '*'].
    *Note* le `42.` dans la liste renvoyée est de type float et non de type
    str.

    *Python est sympa* si s est une chaîne de caractères avec des espaces,
    essayez s.split().
    """
    return []

def calculer_postfixe_lex(lexemes):
    """ Prend en argument une liste de lexèmes représentant une forme
        post-fixée valide et renvoie le résultat correspondant. Par exemple
        calculer_postfixe_lex([5, 6, '+']) renvoie le nombre 11.
    """
    pile_nombres = creer()
    # implementation de l'algorithme vu en cours : on empile les entiers,
    # puis à chaque opération rencontrée, on dépile les les deux derniers
    # éléments de la pile, puis on empile le résultat de l'opération.

    return 0.

def calculer_postfixe(chaine):
    """
    Prend en argument une chaîne de caractères et calcule l'expression
    postfixée passée en argument. Par exemple
    calculer_postfixe('5 6 +') renvoie le flottant 11.

    Indication: il n'y a quasiment rien à faire vous avez déjà fourni tout
    l'effort nécessaire!
    """
    return 0.

print("*****************************")
print("* Bienvenue dans PolaCalc ! *")
print("*****************************")

print()
print("Entrez vos expression en notation polonaise inversée. Exemples:")
for expr in ["5 6 +", "7 3 / 5 6 * -", "5 3 4 + *"]:
    print(expr, "=", calculer_postfixe(expr))

print()
expr = ""
while expr != "quit":
    expr = input("Rentrez votre expression (tapez quit pour quitter) : ")
    if expr != "quit":
        print(expr, "=", calculer_postfixe(expr))

print()
print("Merci d'avoir utilisé PolaCalc, à bientôt !")
