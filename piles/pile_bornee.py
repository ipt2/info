"""
Module implémentant une structure de pile bornée. 

L'utilisateur de ce module doit uniquement utiliser les fonctions
creer, est_vide, est_pleine, empiler, empiler, depiler et sommet.

Ci-dessous est expliqué le fonctionnement interne de la structure de données
sur laquelle l'utilisateur ne doit pas s'appuyer (cette partie est là dans un 
but uniquement pédagogique).

On représente une pile bornée grâce à une liste fixe et un index indiquant
l'index du dernier élément inséré. S'il n'y a aucun élément dans la pile
l'index est à -1.

Par exemple, créer une pile d'une capacité 5 créé la liste suivante (la flèche
représente l'index; ici elle pointe sur -1):
 
    | None | None | None | None | None |
 
  ^
  |


Après avoir inséré un élément (mettons 42), la pile se représente par (la flèche
a avancé) :

    |  42  | None | None | None | None |
 
       ^
       |


Insérons encore un élément (mettons 91), la pile se représente par:

    |  42  |  91  | None | None | None |
 
               ^
               |


Maintenant, dépilons le sommet : la liste reste inchangée, juste
la flèche bouge; la valeur 91 n'est pas effacée (cela coûterait du temps !),
on se souvient qu'elle ne veut "plus rien dire" grâce à la position de la flèche.

    |  42  |  91  | None | None | None |
 
        ^     
        |

Insérons maintenant une nouvelle valeur (mettons 56): la valeur est insérée
en écrasant le 91 précédent.

    |  42  |  56  | None | None | None |
 
              ^     
              |

Si on continue à insérer des valeurs, on obtiendra:

    |  42  |  56  |  86 | 13  | 74 |
 
                                ^     
                                |

Et là, on ne peut plus ajouter de valeurs, la flèche est au bout de la liste !

"""

class PileVide(Exception):
    """Exception indiquant qu'on tente d'accéder à un élément d'une pile vide"""
    pass

class PilePleine(Exception):
    """Exception indiquant qu'on tente d'empiler un élément sur une pile pleine"""
    pass

def creer(capacite):
    """Renvoie une nouvelle pile bornée vide d'une capacité donnée"""
    if type(capacite) != int:
        raise ValueError('capacite doit être un entier')
    if capacite < 0:
        raise ValueError('capacite doit être positif') 

    return {
        'capacite': capacite,
        'elements': [None] * capacite,
        'index_top': -1,
    }


def est_pleine(pile):
    """Renvoie True si la pile est au maximum de sa capacité et False sinon. Ne 
    modifie par la pile."""
    return pile['index_top'] == pile['capacite'] - 1

def est_vide(pile):
    """Renvoie True si la pile est vide, et False sinon. Ne modifie pas la 
    pile."""
    return pile['index_top'] == -1

def empiler(objet, pile):
    """Modifie la pile en ajoutant l'objet à son sommet. Ne renvoie rien."""
    if est_pleine(pile):
        raise PilePleine

    nouvel_index = pile['index_top'] + 1

    pile['elements'][nouvel_index] = objet
    pile['index_top'] = nouvel_index

def depiler(pile):
    """Modifie la pile en retirant l'objet sur le haut de la pile et renvoie cet
    objet. Lance l'erreur PileVide si la   pile est vide. Ne renvoie rien."""
    if est_vide(pile):
        raise PileVide()
    
    index = pile['index_top']
    objet = pile['elements'][index]
    pile['index_top'] = index -1

    return objet



def sommet(pile):
    """Renvoie l'objet au somme de la pile, sans modifier la pile. Lance
    l'erreur PileVide si la   pile est vide."""
    if est_vide(pile):
        raise PileVide()
        
    index = pile['index_top']
    return pile['elements'][index] # renvoie le dernier élément de la liste.



