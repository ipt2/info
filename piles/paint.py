from tkinter import *

class Paint:
    """Paint est une application graphique réalisant un logiciel simple 
    (simpliste ?) de dessin. Pour tracer un tracer un trait, l'utilisateur doit:
        * cliquer dans la fenêtre, 
        * déplacer la souris en maintenant le clic enfoncé,
        * relâcher le clic.
    """

    def __init__(self):
        """Initialise l'application"""

        # self.root représente la fenêtre dans la quelle se déroule notre application
        self.root = Tk()

        # Un "canvas" est une zone graphique dans la quelle on peut faire
        # des dessins. self.canvas désigne le canvas de l'application.
        self.canvas = Canvas(self.root, bg='white', width=600, height=600)
        self.canvas.grid() # on place le canvas


        # On initialise ici les variables utilisées par l'application.
        # Ici on utilise uniquement "last_x" et "last_y", les positions de la 
        # souris lorsque le bouton est enfoncé.
        self.last_x = None
        self.last_y = None

        # Ici on lie ("to bind" en anglais: lier) un événement à une fonction:
        #    * lorsqule le bouton 1 de la souris sera enfoncé (ButtonPress-1),
        #      la fonction on_button_press sera appelée
        #    
        #    * lorsque le bouton 1 de la souris sera relaché (ButtonRelease-1),
        #      la fonction on_button_release sera appelé
        self.canvas.bind('<ButtonPress-1>', self.on_button_press)
        self.canvas.bind('<ButtonRelease-1>', self.on_button_release)

        self.root.mainloop()

    def on_button_press(self, event):
        """Fonction appelée lorsque le bouton de la souris est enfoncé"""

        # On mémorise les coordonnées de la souris
        self.last_x = event.x
        self.last_y = event.y

    def on_button_release(self, event):
        """Fonction appelée lorsque le bouton de la souris est relâché"""
        # On dessine une ligne entre les coordonnées mémorisées précédement
        # et les coordonnées actuelles de la souris.
        self.canvas.create_line(self.last_x, self.last_y, event.x, event.y)
        




if __name__ == '__main__':
    Paint()
