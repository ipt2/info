"""
Module implémentant la structure de pile. 
"""

class PileVide(Exception):
    """Exception indiquant qu'on tente d'accéder à un élément d'une pile vide"""
    pass

def creer():
    """Renvoie une nouvelle pile vide"""
    return []

def empiler(objet, pile):
    """Modifie la pile en ajoutant l'objet à son sommet. Ne renvoie rien."""
    pile.append(objet)

def depiler(pile):
    """Modifie la pile en retirant l'objet sur le haut de la pile et renvoie cet
    objet. Lance l'erreur PileVide si la   pile est vide. Ne renvoie rien."""
    if est_vide(pile):
        raise PileVide()

    return pile.pop()

def sommet(pile):
    """Renvoie l'objet au somme de la pile, sans modifier la pile. Lance
    l'erreur PileVide si la   pile est vide."""
    if est_vide(pile):
        raise PileVide()

    return pile[-1] # renvoie le dernier élément de la liste.


def est_vide(pile):
    """Renvoie True si la pile est vide, et False sinon. Ne modifie pas la 
    pile."""
    return pile == []
