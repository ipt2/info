"""Ce fichier montre comment on peut utiliser le module "pile". Pour cela,
ce fichier doit être dans le même dossier (pour que l'import fonctionne).

Essayer de prédire ce qui va être affiché avant de l'exécuter.

N'hésitez pas à modifier cet exemple pour faire vos tests ! 
"""

# J'importe le module "pile", ainsi je pourrai faire pile.creer, pile.empiler...
import pile


# Je crée une pile vide:
ma_pile = pile.creer()


# J'y mets dedans les valeurs 1, 2, 3 (dans cet ordre):
pile.empiler(1, ma_pile)
pile.empiler(2, ma_pile)
pile.empiler(3, ma_pile)


print("ma_pile après ajouts:", ma_pile) # le "haut" de la pile est à droite

# je regarde au sommet de la pile:
sommet = pile.sommet(ma_pile)
print("sommet après ajouts:", sommet)

if pile.est_vide(ma_pile):
    print("la pile est vide !")
else:
    print("la pile n'est pas vide !")

# j'enlève le sommet:
pile.depiler(ma_pile)
print("pile après premier depiler:", ma_pile)

# j'enlève encore le somme:
sommet2 = pile.depiler(ma_pile)
print("pile après deuxième depiler:", ma_pile, "sommet2:", sommet2)


print("je tente de supprimer encore un élément...")
try:
    sommet3 = pile.depiler(ma_pile)
    print("j'ai réussi ! sommet3 :", sommet3)
except pile.PileVide: # si l'exception PileVide a été lancée
    print("j'ai échoué, la pile était vide")


print("sur ma lancée, je tente à nouveau de supprimer un élément...")
try:
    sommet4 = pile.depiler(ma_pile)
    print("j'ai réussi ! sommet4 :", sommet4)
except pile.PileVide: # si l'exception PileVide a été lancée
    print("j'ai échoué, la pile était vide")


